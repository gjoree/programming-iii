import java.io.*;
import java.net.*;
import java.util.*;

public class P2PChat {
    private static final int RENDEZVOUS_PORT = 54000;
    private static final String RENDEZVOUS_HOST = "localhost";

    private static String username;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your username: ");
        username = scanner.nextLine();

        Thread rendezvousThread = new Thread(() -> {
            try {
                ServerSocket rendezvousServer = new ServerSocket(RENDEZVOUS_PORT);

                while (true) {
                    Socket rendezvousSocket = rendezvousServer.accept();
                    handleIncomingConnection(rendezvousSocket);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        rendezvousThread.start();

        Thread outgoingThread = new Thread(() -> {
            try {
                BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

                while (true) {
                    String message = userInput.readLine();

                    // Skip sending if the message is empty
                    if (message.isEmpty()) {
                        continue;
                    }

                    // Send the message to all connected peers
                    Socket[] connectedPeers = getConnectedPeers();
                    for (Socket peerSocket : connectedPeers) {
                        PrintWriter peerOut = new PrintWriter(peerSocket.getOutputStream(), true);
                        peerOut.println(username + ": " + message);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        outgoingThread.start();
    }

    private static void handleIncomingConnection(Socket rendezvousSocket) {
        try {
            BufferedReader rendezvousIn = new BufferedReader(new InputStreamReader(rendezvousSocket.getInputStream()));
            PrintWriter rendezvousOut = new PrintWriter(rendezvousSocket.getOutputStream(), true);

            // Send the username to the rendezvous server
            rendezvousOut.println(username);

            // Receive the list of connected peers
            String peerListString = rendezvousIn.readLine();
            String[] peerAddresses = peerListString.split(",");

            // Connect to each peer
            for (String peerAddress : peerAddresses) {
                String[] addressParts = peerAddress.split(":");
                String peerHost = addressParts[0];
                int peerPort = Integer.parseInt(addressParts[1]);

                Socket peerSocket = new Socket(peerHost, peerPort);
                handlePeerConnection(peerSocket);
            }

            // Listen for incoming messages from other peers
            Thread incomingThread = new Thread(() -> {
                try {
                    while (true) {
                        String message = rendezvousIn.readLine();
                        System.out.println(message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            incomingThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void handlePeerConnection(Socket peerSocket) {
        try {
            BufferedReader peerIn = new BufferedReader(new InputStreamReader(peerSocket.getInputStream()));
            PrintWriter peerOut = new PrintWriter(peerSocket.getOutputStream(), true);

            // Send the username to the peer
            peerOut.println(username);

            // Listen for incoming messages from the peer
            Thread incomingThread = new Thread(() -> {
                try {
                    while (true) {
                        String message = peerIn.readLine();
                        System.out.println(message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            incomingThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Socket[] getConnectedPeers() {
        try {
            Socket rendezvousSocket = new Socket(RENDEZVOUS_HOST, RENDEZVOUS_PORT);
            BufferedReader rendezvousIn = new BufferedReader(new InputStreamReader(rendezvousSocket.getInputStream()));
            PrintWriter rendezvousOut = new PrintWriter(rendezvousSocket.getOutputStream(), true);

            // Send the username to the rendezvous server
            rendezvousOut.println(username);

            // Receive the list of connected peers
            String peerListString = rendezvousIn.readLine();
            String[] peerAddresses = peerListString.split(",");

            // Connect to each peer and return the sockets
            List<Socket> connectedPeers = new ArrayList<>();
            for (String peerAddress : peerAddresses) {
                String[] addressParts = peerAddress.split(":");
                String peerHost = addressParts[0];
                int peerPort = Integer.parseInt(addressParts[1]);

                Socket peerSocket = new Socket(peerHost, peerPort);
                connectedPeers.add(peerSocket);
            }

            return connectedPeers.toArray(new Socket[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Socket[0];
    }
}
