import java.io.*;
import java.net.*;

public class Peer {
    private int listenerPort;

    public Peer() {
        // Dynamically assign a random available port for the listener
        try (ServerSocket socket = new ServerSocket(0)) {
            listenerPort = socket.getLocalPort();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(String serverHost, int serverPort) {
        try {
            // Connect to the server
            Socket serverSocket = new Socket(serverHost, serverPort);
            System.out.println("Connected to server");

            // Start the listener to receive messages from other peers
            Thread listenerThread = new Thread(() -> {
                try {
                    ServerSocket listenerSocket = new ServerSocket(listenerPort);
                    System.out.println("Listener started on port " + listenerPort);

                    while (true) {
                        Socket peerSocket = listenerSocket.accept();
                        System.out.print("qweqweqw");
                        Thread peerThread = new Thread(() -> handlePeer(peerSocket));
                        peerThread.start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            listenerThread.start();

            // Send messages to the server
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            PrintWriter serverWriter = new PrintWriter(serverSocket.getOutputStream(), true);
            String message;
            while ((message = reader.readLine()) != null) {
                serverWriter.println(message);
            }

            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handlePeer(Socket peerSocket) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(peerSocket.getInputStream()));
            String message;
            while ((message = reader.readLine()) != null) {
                System.out.println("Received message from peer: " + message);
            }

            peerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String serverHost = "localhost"; // Replace with the server's host address
        int serverPort = 5000; // Replace with the server's port number

        Peer peer = new Peer();
        peer.start(serverHost, serverPort);
    }
}
